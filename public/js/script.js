
$(function () {
  $(document).scroll(function () {
    var $main_nav = $(".main-nav");
    $main_nav.toggleClass('scrolled', $(this).scrollTop() > ($main_nav.height() + $(".main-header").height()));
    var $menu = $(".nav");
    // $menu.toggleClass('scrolled');
    $menu.toggleClass('scrolled', $(this).scrollTop() > 0);
  });
});

